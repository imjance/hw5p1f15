from flask import Flask, request
from coreapp import app
from datetime import date

index_page ="""
<DOCTYPE! html>
<html lang="en-US">
<head>
    <title>Hello World Page</title>
    <meta charset=utf-8">
</head>
<body>
    <h1>Homework 5 Problem 1</h1>
    <p>Find the day of the week:</p>
    <form action="/dow" method="get">
    	{0}
	    <input value="Submit" id="submit" type="submit"/> 
	</form>
</body>
</html>
"""

dow_page = """
<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Hello World Page</title>
    <meta charset=utf-8">
</head>
<body>
    <h2>Day of week: {0}</h2>
</body>
</html>
"""

@app.route('/')
def index():
	months = "<select name='months' id='months'>"
	for i in range(1,13):
		months += "<option value='"+str(i)+"'>" +str(i) + "</option>"
	months += "</select>"

	days = "<select name='days' id='days'>"
	for i in range(1,32):
		days += "<option value='"+str(i)+"'>" +str(i) + "</option>"
	days += "</select>"

	years = "<select name='years' id='years'>"
	for i in range(2014,2021):
		years += "<option value='"+str(i)+"'>" +str(i) + "</option>"
	years += "</select>"

	data = months + days + years

	return index_page.format(data)

  
@app.route('/dow')    
def other():
	month = int(request.args.getlist('months')[0])
	day = int(request.args.getlist('days')[0])
	year = int(request.args.getlist('years')[0])

	try:
		data = date(year,month,day).isoweekday()
		if data == 1:
			day_of_week = "Monday"
		elif data == 2:
			day_of_week = "Tuesday"
		elif data == 3:
			day_of_week = "Wednesday"
		elif data == 4:
			day_of_week = "Thursday"
		elif data == 5:
			day_of_week = "Friday"
		elif data == 6:
			day_of_week = "Saturday"
		else:
			day_of_week = "Sunday"

	except Exception as e:
		error_msg = str(e)
		return dow_page.format(error_msg)

	return dow_page.format(day_of_week)
    

